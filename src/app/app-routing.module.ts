import { ServerErrorComponent } from './server-error/server-error.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

import { ProductsComponent } from './products/component/products/products.component';
import { LoginComponent } from './customers/components/login/login.component';
import { RegistrationComponent } from './customers/components/registration/registration.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path:'', redirectTo:'/login',pathMatch:'full'},
  {path:'registration', component:RegistrationComponent},
  {path:'login',component:LoginComponent},
  {path:'error',component:ServerErrorComponent},
  {path:'home',loadChildren:'./products/products.module#ProductsModule'},
 {path:'**', component:PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
