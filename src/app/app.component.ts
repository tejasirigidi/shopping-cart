import { LoadingService } from './loading.service';

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'shopping';
  constructor(private loader:LoadingService){

  }
  loading$=this.loader.loading$;
}
