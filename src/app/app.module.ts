
import { NetworkInterceptor } from './network.interceptor';
import { OrdersModule } from './orders/orders.module';
import { LoginComponent } from './customers/components/login/login.component';
import { CartService } from './cart/service/cart.service';
import { CustomersModule } from './customers/customers.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule} from '@angular/forms';
import { CustomerService } from './customers/services/customer.service';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatProgressSpinnerModule } from '@angular/material';
import { ServerErrorComponent } from './server-error/server-error.component';

@NgModule({
  declarations: [
    AppComponent,
    PagenotfoundComponent,
    ServerErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    CustomersModule,
    OrdersModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatProgressSpinnerModule
  ],
  providers: [CustomerService,
  CartService,
LoginComponent,
{
  provide:HTTP_INTERCEPTORS,
  useClass:NetworkInterceptor,
  multi:true
}],
  bootstrap: [AppComponent]
})
export class AppModule { }
