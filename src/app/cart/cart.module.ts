import { NetworkInterceptor } from './../network.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CartService } from './service/cart.service';
import { CartComponent } from './component/cart/cart.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule, MatButtonModule, MatDialogModule, MatToolbarModule } from '@angular/material';
import { OrderDialogComponent } from './component/order-dialog/order-dialog.component';

@NgModule({
  declarations: [
    CartComponent,
    OrderDialogComponent
  ],
  entryComponents:[OrderDialogComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatDialogModule,
    MatToolbarModule
  ],
  providers:[
    CartService,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:NetworkInterceptor,
      multi:true
    }
  ]
})
export class CartModule { }
