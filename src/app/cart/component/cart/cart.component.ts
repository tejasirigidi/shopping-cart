import { OrderDialogComponent } from './../order-dialog/order-dialog.component';
import { LoadingService } from './../../../loading.service';
import { CustomerService } from 'src/app/customers/services/customer.service';
import { Router, ActivatedRoute} from '@angular/router';
import { CartService } from './../../service/cart.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog,MatDialogConfig } from '@angular/material';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  id:number
  items=[]
  cost:number=0;
  laptopCount:number
  orderCount:number;
  quantity:number=0;
  loading$=this.loader.loading$
  constructor(private service:CartService,private router: Router,private rout: ActivatedRoute,private customerService: CustomerService,
      private loader:LoadingService,
      public dialog:MatDialog) { }
/**
 * getting the cart items from the cart service
 */
  ngOnInit() {

    this.id=this.customerService.customerId
    
    this.service.getCartIteams(this.id)
    .subscribe((response)=>{
      console.log(response)
      this.items=response
      
      this.cost=0
       this.items.forEach(element=>{
       
          this.cost=this.cost+element.products.productCost*element.products.productQuantity    
    
       })
      })
       
  }
/**
 * placing order and deleting the cart items
 */
  

placeOrder(){
  
  this.items.forEach(element=>{
    let value={
      
        customerId: this.id,
        products: {
          id: element.products.id,
          productName: element.products.productName,
          productCost:element. products.productCost,
          productImage:element.products.productImage,
          productQuantity:element.products.productQuantity
        }
      
    }
    this.service.postOrders(value)
    .subscribe((response)=>{
      console.log(response)
    })

  })

this.dialog.open(OrderDialogComponent,{
  height: '300px',
  width: '400px',
});
 
}

/**
 * deleting the selected cart items in the cart page 
 */
  
deleteItem(data){
  if(data.products.productQuantity>1){
    console.log("delete")
    let value={
      customerId: this.id,
      products: {
        id:data.products.id,
        productName:data.products.productName,
        productCost: data.products.productCost,
        productImage:data.products.productImage,
        productQuantity:data.products.productQuantity-1
      }
    }
    this.service.updateCart(data.id,value)
    .subscribe(response=>console.log(response))
    this.ngOnInit()
  }
  else{
    this.service.deleteCart(data.id)
    .subscribe((response)=>{
      console.log(response)
      this.ngOnInit()
    })
  }


  
 

}
}

