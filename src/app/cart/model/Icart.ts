export interface Icart{
        customerId: number,
        products: {
          id: number,
          productName: string,
          productCost: number,
          productImage:string,
          productQuantity:number
        },
        id:number
      
}