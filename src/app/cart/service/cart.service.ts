import { Iorders } from './../../orders/model/Iorders';
import { Icart } from './../model/Icart';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CartService {

  constructor(private http:HttpClient) { }

 /**
  * 
  * @param id =customer id
  * @returns particular customer cart items
  */

getCartIteams(id) : Observable<Icart[]>{
  let url="http://localhost:3000/cart?customerId="+id;
  return this.http.get<Icart[]>(url);
}
/**
 * 
 * @param data =selected items by customer
 * @returns posted data in orders
 */
postOrders(data): Observable<Iorders[]>{
  let url="http://localhost:3000/orders"
  return this.http.post<Iorders[]>(url,data)
}
/**
 * 
 * @param id = selected id has to delete
 * @returns deleted confirmation
 */

deleteCart(id){
let url="http://localhost:3000/cart/"+id;
return this.http.delete(url)
}

updateCart(id,data){
  let url="http://localhost:3000/cart/"+id;
  return this.http.put(url,data)
}


}
