import { LoadingService } from './../../../loading.service';
import { Icustomer } from './../../model/Icustomer';

import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CustomerService } from 'src/app/customers/services/customer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  data:Icustomer[];
  constructor(private router: Router, private service: CustomerService,
    private loader:LoadingService) { }

    
loading$=this.loader.loading$

public customerId:number;
/**
 * gets the registered customer details
 */
  ngOnInit() {
    this.service.getCustomer()
    .subscribe((response) => {
    this.data=response;
      console.log(this.data)
   
    })
  }

 /**
  * this is the form group
  */
 loginForm= new FormGroup({
    username:new FormControl(),
    password:new FormControl()
  });
  userNameInValid:boolean=false;
  passwordInValid:boolean=false;
  userNameValid:boolean=false;
  passwordValid:boolean=false;
  userMessage:string="";
  passwordMessage:string="";
  accountMessage:string="";
  accountInValid:boolean=false;
/**
 * this registration function navigates to registration page 
 * once you click on the registration button
 */
  registration(){
    this.router.navigate(['/registration']);
  }
  /**
   * onSubmit function get the customer details and check 
   * wheather the username and password are matching to existing ones
   * otherwise it will raise an alert to register
   */

  onSubmit(){
    
    
       this.data.forEach(element => {
        
         if(element.Username==this.loginForm.get('username').value && element.Password == this.loginForm.get('password').value){
            this.userNameValid=true
          
           this.passwordValid=true
            this.service.getId(element.id)
           this.router.navigate(['/home'])
         }else if(element.Username==this.loginForm.get('username').value){
            this.userNameValid=true
         }
         else if(element.Password == this.loginForm.get('password').value){
           this.passwordValid=true
         }
         
       
       });

       if(this.userNameValid==false){
         this.userNameInValid=true
         this.userMessage="Incorrect Username"
       }
       if(this.passwordValid==false){
         this.passwordInValid=true
        this.passwordMessage="Incorrect Password"
      }
      if(this.userNameValid==false && this.passwordValid==false){
        this.accountInValid=true
        this.accountMessage="Account not Found"
        this.passwordInValid=false
        this.userNameInValid=false
      }
      
  }

}
