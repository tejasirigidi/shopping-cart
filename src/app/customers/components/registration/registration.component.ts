import { LoadingService } from './../../../loading.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/customers/services/customer.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private service : CustomerService,private router :Router,private loader:LoadingService) { }

  
loading$=this.loader.loading$
  ngOnInit() {
  }
/**
 * this the form group of registration 
 */
  registrationForm =new FormGroup({
    Username: new FormControl(),
    Mobile:new FormControl(),
    Gender:new FormControl(),
    Pincode:new FormControl(),
    Password:new FormControl()
  });
/**
 * this function sends the registration 
 */
  onSubmit(){
    this.service.registerCustomer(this.registrationForm.value)
    .subscribe(response=>{console.log(response)
    this.router.navigate(['login'])})
    
  
    
  }

}
