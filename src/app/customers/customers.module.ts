import { NetworkInterceptor } from './../network.interceptor';
import { CustomerService } from 'src/app/customers/services/customer.service';

import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatProgressSpinnerModule, MatButtonModule, MatToolbarModule } from '@angular/material';


@NgModule({
  declarations: [
    LoginComponent,
    RegistrationComponent,
  
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatToolbarModule
  ],
  providers:[
    CustomerService,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:NetworkInterceptor,
      multi:true
    }
  ]
})
export class CustomersModule { }
