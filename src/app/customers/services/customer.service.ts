import { Icustomer } from './../model/Icustomer';
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';


@Injectable()
export class CustomerService {

  customerId:number

  getId(id){
    this.customerId=id;
  }



  constructor(private http: HttpClient) { }
  url=" http://localhost:3000/customer";

  /**
   * 
   * @param data =entered data in the registration form
   * @returns posted data in the server
   */
  registerCustomer(data) :Observable<Icustomer>{
    return this.http.post<Icustomer>(this.url,data);
  }

  /**
   * 
   * @returns the customer details present in the db
   */
  getCustomer() :Observable<Icustomer[]>{
    return this.http.get<Icustomer[]>(this.url)
    
                          
    
  }
}
