import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  /**
   * function that navigates to prducts page
   */
  products(){
    this.router.navigate(['home/products']);
  }
  /**
   * function that navigates to carts page
   */
  cart(){
    this.router.navigate(['home/cart']);
  }
   /**
   * function that navigates to orders page
   */
  orders(){
    this.router.navigate(['home/orders']);
  }
}
