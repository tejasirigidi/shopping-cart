import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpInterceptor,
     HttpRequest,
     HttpEvent,
     HttpHandler,
     HttpErrorResponse
                 } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {finalize,catchError} from 'rxjs/operators'
import { LoadingService } from './loading.service';

@Injectable()
export class NetworkInterceptor implements HttpInterceptor{
constructor(private loader:LoadingService,private router:Router){

}
intercept(request:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>{
   this.loader.show()
    return next.handle(request).pipe(
        catchError((error: HttpErrorResponse) => {
        let errorMsg = 'server error';
       
        this.router.navigate(['error'])
        console.log(errorMsg);
        return throwError(errorMsg);
      }),
     finalize(()=>{
         this.loader.hide()
      } )
    )
    
}

}

