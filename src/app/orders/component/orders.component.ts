import { LoadingService } from './../../loading.service';
import { Router } from '@angular/router';
import { Iorders } from './../model/Iorders';
import { CustomerService } from 'src/app/customers/services/customer.service';
import { OrdersService } from './../service/orders.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
id:number
data:Iorders
  constructor(private service: OrdersService,private customerService: CustomerService,private router:Router,private loader:LoadingService) { }
  loading$=this.loader.loading$

  /**
   * gets data about placed orders
   */
  ngOnInit() {
    this.id=this.customerService.customerId

    this.service.getOrders(this.id)
    .subscribe((response)=>{
      this.data=(response)
      console.log(this.data)
    })
  }
  /**
   * function that returns to home page
   */
  home(){
    this.router.navigate(['/home'])
  }

}
