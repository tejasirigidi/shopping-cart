import { NetworkInterceptor } from './../network.interceptor';
import { OrdersService } from './service/orders.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './component/orders.component';
import { MatProgressSpinnerModule, MatButtonModule, MatToolbarModule } from '@angular/material';

@NgModule({
  declarations: [OrdersComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatToolbarModule
  ],
  providers:[
    OrdersService,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:NetworkInterceptor,
      multi:true
    }
  ]
})
export class OrdersModule { }
