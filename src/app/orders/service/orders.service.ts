import { Observable } from 'rxjs';
import { Iorders } from './../model/Iorders';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private http:HttpClient) { }
/**
 * 
 * @param id =customer id
 * @returns order items that placed by the customer
 */
  getOrders(id):Observable<Iorders>{
    let url="http://localhost:3000/orders?customerId="+id
    return this.http.get<Iorders>(url)
  }

}
