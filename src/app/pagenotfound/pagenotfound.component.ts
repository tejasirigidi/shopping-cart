import { Router } from '@angular/router';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pagenotfound',
  template: `
  
     <h2> pagenotfound </h2>
     <p>
     <button (click)="home()" class="btn" mat-button color="primary">Home</button>
 </p>
  `,
  styles: []
})
export class PagenotfoundComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  home(){
    this.router.navigate(['login'])
  }

}
