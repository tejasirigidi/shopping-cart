import { LoadingService } from './../../../loading.service';
import { CustomerService } from 'src/app/customers/services/customer.service';
import { Iproducts } from './../../model/Iproducts';
import { ActivatedRoute, Router} from '@angular/router';
import { ProductsService } from './../../service/products.service';
import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private service: ProductsService,private router: Router,private rout: ActivatedRoute, private customerService: CustomerService) { }
id: number
 
customerName :string
quantity:string="1"
products :Iproducts[]
cartIteams=[]
data=[]

/**
 * gets the products details from the server
 */

  ngOnInit() {
   
    this.id=this.customerService.customerId;
    
    this.service.getName(this.id)
    .subscribe((response)=>{
      console.log(response)
      this.customerName=response.Username;
    })

    
    this.service.getProducts()
    .subscribe((response)=>{
      this.products=response;
    })

    this.service.getCartIteams(this.id)
.subscribe((response)=>{
  this.cartIteams=response;
  
})

}



/**
 * 
 * @param products =selected items that are placed in the cart
 */
addToCart(products){
let value={}
let itemRepeated:Boolean=false
    this.cartIteams.forEach(element=>{
      if(element.products.id==products.id){
        value={
          customerId: this.id,
          products: {
            id: products.id,
            productName: products.productName,
            productCost: products.productCost,
            productImage:products.productImage,
            productQuantity:parseInt(element.products.productQuantity)+1
          }
        }
        
        itemRepeated=true
        this.service.updateCartItem(element.id,value)
        .subscribe((response)=>{
          console.log(response)
        })

      }

    })
   
  
  if(itemRepeated==false){
    value={
      customerId: this.id,
      products: {
        id: products.id,
        productName: products.productName,
        productCost: products.productCost,
        productImage:products.productImage,
        productQuantity:this.quantity
      }
        
     }
   this.service.postCartIteams(value)
   .subscribe((response)=>console.log(response))
   this.ngOnInit()
     
  }
}
/**
 * function navigates to carts page
 */
Cart(){
  this.router.navigate(['home/cart'])
}
/**
 * function navigates to orders page
 */
Orders(){
  this.router.navigate(['home/orders'])

}

}
