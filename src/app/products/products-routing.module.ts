import { HomeComponent } from './../home/component/home.component';
import { OrdersComponent } from './../orders/component/orders.component';
import { CartComponent } from './../cart/component/cart/cart.component';
import { ProductsComponent } from './component/products/products.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  {path:'',component:HomeComponent},
  {path:'products',component:ProductsComponent},
  {path:'cart',component:CartComponent},
  {path:'orders',component:OrdersComponent}

  


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
