import { OrdersModule } from './../orders/orders.module';
import { HomeComponent } from './../home/component/home.component';
import { CartModule } from './../cart/cart.module';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductsService } from './service/products.service';
import { ProductsComponent } from './component/products/products.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule, MatButtonModule, MatToolbarModule } from '@angular/material';

@NgModule({
  declarations: [
    ProductsComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    CartModule,
    OrdersModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatToolbarModule
    
  ],
  providers:[
    ProductsService
  ]
})
export class ProductsModule { }
