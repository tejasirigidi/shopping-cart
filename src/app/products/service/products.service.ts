import { LoadingService } from './../../loading.service';
import { Icart } from './../../cart/model/Icart';
import { Iproducts } from './../model/Iproducts';
import { Observable } from 'rxjs';
import { Icustomer } from './../../customers/model/Icustomer';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ProductsService {

  constructor(private http: HttpClient,private loader:LoadingService) { }
 /**
  * 
  * @param id =customer id
  * @returns details of customer
  */
getName(id):Observable<Icustomer>{
  let idUrl="http://localhost:3000/customer/"+id;
  return this.http.get<Icustomer>(idUrl);
}
/**
 * 
 * @returns poducts details from server
 */
getProducts():Observable<Iproducts[]>{
  let Purl=" http://localhost:3000/products"
  return this.http.get<Iproducts[]>(Purl)
}
/**
 * 
 * @param data =selected items by customer
 * @returns posted items in the cart
 */
postCartIteams(data):Observable<Icart[]>{
  let cart="http://localhost:3000/cart";
  return this.http.post<Icart[]>(cart,data);
}

getCartIteams(id) : Observable<Icart[]>{
  let url="http://localhost:3000/cart?customerId="+id;
  return this.http.get<Icart[]>(url);
}

updateCartItem(id,data): Observable<Icart[]>{
  let url="http://localhost:3000/cart/"+id;
  return this.http.put<Icart[]>(url,data);
}

}
