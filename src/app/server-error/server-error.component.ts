import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.css']
})
export class ServerErrorComponent implements OnInit {

  constructor(private router:Router) { }
imageSource="https://wpgeodirectory.com/wp-content/uploads/2016/06/oops.png";
  ngOnInit() {
  }
  home(){
    this.router.navigate(["login"])
}
}
